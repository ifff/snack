import java.util.ArrayList;

// An incredibly fancy test suite for snack stuff.
public class Test {
    private ArrayList<Result> results;
    private String desc;

    public Test(String desc) {
	this.desc = desc;
	this.results = new ArrayList<Result>();
    }

    public <T> Test expect_from(String testStr, T testValue, T correctValue) {
	boolean passed = testValue.equals(correctValue);
	Result res = new Result(testStr, testValue+"", passed);
	results.add(res);
	return this;
    }

    public void conclude() {
	System.out.print("Results of test: ");
	System.out.println("'" + desc + "'");

	for (Result result : results)
	    System.out.println(result);
    }

    private class Result {
	private String desc, result;
	private boolean passed;
	
	public Result(String desc, String result, boolean passed) {
	    this.desc = desc;
	    this.result = result;
	    this.passed = passed;
	}

	public String toString() {
	    String str = "";
	    
	    str += "[" + (passed? '\u2713' : '\u2717') + "]";
	    str += " '" + desc + "' ";
	    str += passed? "passed" : "failed";
	    str += " with result";
	    str += " '" + result + "'.";
	    
	    return str;
	}
    }
}
