import java.util.ArrayList;

// A runner class for testing the result of compiled snack programs.
public class Runner {
    public static void main(String[] args) {
        System.out.println("**** testing snack program ****");
	
	System.out.println();
	
	Test basic_functions = new Test("Basic Functions");
	basic_functions
	    .expect_from("Tasty.one()", Tasty.one(), 1)
	    .expect_from("Tasty.negative()", Tasty.negative(), -5)
	    .expect_from("Tasty.max_int()", Tasty.max_int(), Integer.MAX_VALUE)
	    .expect_from("Tasty.min_int()", Tasty.min_int(), Integer.MIN_VALUE+1)
	    .expect_from("Tasty.int_div()", Tasty.int_div(), 2)
	    .expect_from("Tasty.int_mul()", Tasty.int_mul(), 10)
	    .expect_from("Tasty.int_rem()", Tasty.int_rem(), 1)
	    .expect_from("Tasty.pi()", Tasty.pi(), (float)3.14159)
	    .expect_from("Tasty.float_0()", Tasty.float_0(), (float)24524.0)
	    .expect_from("Tasty.float_1()", Tasty.float_1(), (float)-0.01)
	    .expect_from("Tasty.fadd(1.5, 0.7)",
			 Tasty.fadd((float)1.5, (float)0.7), (float)2.2)
	    .expect_from("Tasty.fsub(1.5, 0.7)",
			 Tasty.fsub((float)1.5, (float)0.7), (float)0.8)
	    .expect_from("Tasty.fmul(0.5, 2.0)",
			 Tasty.fmul((float)0.5, (float)2.0), (float)1.0)
	    .expect_from("Tasty.frem(1.5, 0.5)",
			 Tasty.frem((float)1.5, (float)0.5), (float)0.0)
	    .expect_from("Tasty.int_to_float(1)", Tasty.int_to_float(1), (float)1.0)
	    .expect_from("Tasty.float_to_int(1.5)", Tasty.float_to_int((float)1.5), 1)
	    .expect_from("Tasty.float_to_char(133.7)",
			 Tasty.float_to_char((float)133.7), (char)((int)133.7))
	    .expect_from("Tasty.inc(1)", Tasty.inc(1), 2)
	    .expect_from("Tasty.add(1, 2)", Tasty.add(1, 2), 3)
	    .expect_from("Tasty.foo(1, 2, 3)", Tasty.foo(1, 2, 3), 2)
	    .expect_from("Tasty.local_test(1)", Tasty.local_test(1), -1)
	    .expect_from("Tasty.inc_wrapped(1)", Tasty.inc_wrapped(1), 2)
	    .expect_from("Tasty.quadruple(3)", Tasty.quadruple(3), 12)
	    .expect_from("Tasty.two()", Tasty.two(), 2)
	    .conclude();

	System.out.println();
	
	Test simple_booleans = new Test("Simple Boolean Logic");
	simple_booleans
	    .expect_from("Tasty.not(true)", Tasty.not(true), false)
	    .expect_from("Tasty.frob(^f, ^f)", Tasty.frob(false, false), false)
	    .expect_from("Tasty.frob(^f, ^t)", Tasty.frob(false, true), true)
	    .expect_from("Tasty.no(^t)", Tasty.no(true), false)
	    .expect_from("Tasty.or(^f, ^t)", Tasty.or(false, true), true)
	    .expect_from("Tasty.or(^t, ^t)", Tasty.or(true, true), true)
	    .expect_from("Tasty.or(^f, ^f)", Tasty.or(false, false), false)
	    .expect_from("Tasty.and(^f, ^f)", Tasty.and(false, false), false)
	    .expect_from("Tasty.and(^f, ^t)", Tasty.and(false, true), false)
	    .expect_from("Tasty.and(^t, ^t)", Tasty.and(true, true), true)
	    .conclude();

	System.out.println();
	
	Test characters = new Test("Characters");
	characters
	    .expect_from("Tasty.latin_letter()", Tasty.latin_letter(), 'A')
	    .expect_from("Tasty.symbol()", Tasty.symbol(), '%')
	    .expect_from("Tasty.newline()", Tasty.newline(), '\n')
	    .expect_from("Tasty.backslash()", Tasty.backslash(), '\\')
	    .expect_from("Tasty.tick()", Tasty.tick(), '\'')
	    .expect_from("Tasty.quote()", Tasty.quote(), '\"')
	    .expect_from("Tasty.unicode()", Tasty.unicode(), '\u1a01')
	    .expect_from("Tasty.char_from_int()", Tasty.char_from_int(), '\u0539')
	    .conclude();

	System.out.println();
	
	Test comparison = new Test("Comparison");
	comparison
	    .expect_from("Tasty.eq(1, 2)", Tasty.eq(1, 2), false)
	    .expect_from("Tasty.neq(1, 2)", Tasty.neq(1, 2), true)
	    .expect_from("Tasty.never_true(1, 2)", Tasty.never_true(1, 2), false)
	    .expect_from("Tasty.always_true(1, 2)", Tasty.always_true(1, 2), true)
	    .expect_from("Tasty.lt(1, 2)", Tasty.lt(1, 2), true)
	    .expect_from("Tasty.gt(1, 2)", Tasty.gt(1, 2), false)
	    .expect_from("Tasty.lt(1, 1)", Tasty.lt(1, 1), false)
	    .expect_from("Tasty.gt(1, 1)", Tasty.gt(1, 1), false)
	    .expect_from("Tasty.lte(1, 2)", Tasty.lte(1, 2), true)
	    .expect_from("Tasty.lte(1, 1)", Tasty.lte(1, 1), true)
	    .expect_from("Tasty.gte(1, 2)", Tasty.gte(1, 2), false)
	    .expect_from("Tasty.eq(1, 1)", Tasty.eq(1, 1), true)
	    .conclude();

	System.out.println();

	Test conditionals = new Test("Conditionals");
        conditionals
	    .expect_from("Tasty.eq(1, 2)", Tasty.eq(1, 2), false)
	    .expect_from("Tasty.haha(0)", Tasty.haha(0), 1)
	    .expect_from("Tasty.haha(1)", Tasty.haha(1), 1)
	    .expect_from("Tasty.haha(2)", Tasty.haha(2), 1)
	    .expect_from("Tasty.haha(3)", Tasty.haha(3), 2)
	    // .expect_from("Tasty.sum(-1)", Tasty.sum(-1), 0)
	    // .expect_from("Tasty.sum(0)", Tasty.sum(0), 0)
	    // .expect_from("Tasty.sum(2)", Tasty.sum(2), 3)
	    // .expect_from("Tasty.sum(3)", Tasty.sum(3), 6)
	    // .expect_from("Tasty.sum(4)", Tasty.sum(4), 10)
	    .conclude();

	System.out.println();

	Test strings = new Test("Strings");
	strings
	    .expect_from("Tasty.a_string()", Tasty.a_string(), "\"he\nllo there!")
	    .conclude();
	
	System.out.println();

	// Test lists = new Test("Lists");
	// ArrayList<String> hi = new ArrayList<String>();
	// hi.add("hello");
	// hi.add("world");
	// lists
	//     .expect_from("Tasty.str_list()", Tasty.str_list(), hi)
	//     .conclude();

	System.out.println();

	//Tasty.hello();
	
	System.out.println();
	
        System.out.println("**** looks like it didn't crash! did it work? ****");
    }
}
