/**
 * Copyright (C) 2020 by Benjamin Duchild
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt;

#[allow(dead_code)]
#[derive(Clone, Debug, PartialEq)]
pub enum Term {
    I32(i32),
    F32(f32),
    Bool(bool),
    Char(char),
    Str(String),
    List(Vec<Box<Term>>),
    Void,
    Nil,
    Builtin(Op),
    App(Box<Term>, Vec<Box<Term>>),
    Define(String, Box<Term>),
    Ident(String),
    Fn(Vec<String>, Vec<Term>),
    Cond(Vec<(Box<Term>, Vec<Box<Term>>)>, Vec<Box<Term>>),
}

impl fmt::Display for Term {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Term::*;
        
        fn commasep(args: &Vec<Box<Term>>) -> String {
            let mut s = String::new();
            let mut i = 0;

            for arg in args {
                i += 1;

                if i == args.len() {
                    s.push_str(format!("{}", *arg).as_str());
                } else {
                    s.push_str(format!("{}, ", *arg).as_str());
                }
            }

            s
        }
        
	let s = match &self {
	    I32(n) => format!("{}", n),
            F32(n) => format!("{}", n),
	    Bool(b) => format!("{}", if *b { "^t" } else { "^f" } ),
            Char(c) => format!("'{}'", c),
            Str(s) => format!("\"{}\"", s),
            List(terms) => format!("{{{}}}", commasep(terms)),
	    Void => String::from("<void>"),
            Nil => String::from("<nil>"),
	    Builtin(op) => format!("{}", op),
	    Define(name, t) => format!("let {} = {}", name, t),
	    Ident(name) => format!("{}", name),
	    Fn(_, _) => String::from("<fn>"),
            Cond(_, _) => String::from("<conditional>"),
            App(abs, args) => match **abs {
                Builtin(Op::Not) if args.len() == 1 =>
                    format!("!{}", args[0]),
                Builtin(ref op) if args.len() == 2 =>
                    format!("{} {} {}", args[0], op, args[1]),
                Ident(ref name) => format!("{}({})", name, commasep(&args)),
                _ => String::from("<app>")
            }
	};

	write!(f, "{}", s)
    }
}

#[allow(dead_code)]
#[derive(Clone, Debug, PartialEq)]
pub enum Op {
    // int arith
    Add,
    Sub,
    Mul,
    Div,
    Rem,
    // float arith
    Fadd,
    Fsub,
    Fmul,
    Fdiv,
    Frem,
    // bool
    Or,
    And,
    Not,
    // cmp
    Eq,
    Neq,
    Lt,
    Lte,
    Gt,
    Gte,
    // typecasts
    I2C,
    I2F,
    F2I,
}

// this is a comment.
/// this is a documentation comment.
#[allow(dead_code)]
impl fmt::Display for Op {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
	let s = match *self {
	    Op::Add => "+",
	    Op::Sub => "-",
            Op::Mul => "*",
            Op::Div => "/",
            Op::Rem => "%",
            Op::Fadd => "+.",
	    Op::Fsub => "-.",
            Op::Fmul => "*.",
            Op::Fdiv => "/.",
            Op::Frem => "%.",
	    Op::Or => "||",
	    Op::And => "&&",
	    Op::Not => "!",
	    Op::Eq => "==",
	    Op::Neq => "!=",
	    Op::Lt => "<",
	    Op::Lte => "<=",
	    Op::Gt => ">",
	    Op::Gte => ">=",
            Op::I2C => "%i2c",
            Op::I2F => "%i2f",
            Op::F2I => "%f2i",
	};

	write!(f, "{}", s)
    }
}

// Useful for keeping track of variables/type variables/etc
#[derive(Debug, Copy, Clone)]
pub struct Counter(u16);
#[allow(dead_code)]
impl Counter {
    pub fn new() -> Counter {
        Counter(0)
    }

    /// Increment the counter and return the value it /was/ at.
    /// This is kind of like the *postfix* ++ operator in C.
    pub fn inc(&mut self) -> u16 {
        let n = self.0;
        self.0 += 1;
        n
    }

    /// Get the current value of the counter.
    pub fn get(&self) -> u16 {
        self.0
    }

    /// Reset the counter to 0.
    pub fn reset(&mut self) {
        self.0 = 0;
    }

    /// Rewind the counter back by the given number.
    /// If this number is greater than the counter's current
    /// value, then the counter gets reset.
    pub fn rewind(&mut self, n: u16) {
        if n >= self.0 {
            self.reset();
        } else {
            self.0 -= n;
        }
    }

    /// Skip the count forward by the given number.
    pub fn skip(&mut self, n: u16) {
        self.0 += n;
    }
}
