/**
 * SNACK is a little experimental programming language that is run via a bytecode VM.
 *
 * Copyright (C) 2020 by Benjamin Duchild
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

extern crate jvm_assembler;
extern crate pest;
#[macro_use]
extern crate pest_derive;

use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

mod compiler;
mod errors;
mod parser;
mod symtab;
mod typechecker;
mod util;
mod prelude;

fn main() -> std::io::Result<()> {    
    let args: Vec<String> = env::args().collect();
    if args.len() == 3 && args[1] == "snc" {
        // read in a file from the command-line arguments
        let fpath = Path::new(&args[2]);
        compile_program(fpath)?;
    } else {
        println!("Usage: \n\tcargo run snc <source file>");
        println!("Aborting.");
    }

    Ok(())
}

fn compile_program(fpath: &Path) -> std::io::Result<()> {
    use polytype::*;
    use jvm_assembler::Java as JavaType;
    
    // read the file contents into a string
    let mut file = File::open(fpath)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    // create a prelude
    let mut prelude = prelude::Prelude::new();
    prelude.add_method(String::from("println"), String::from("java/io/PrintStream"),
                       tp!(@arrow[tp!(str), tp!(void)]),
                       vec![JavaType::Class("java/lang/String")], JavaType::Void);
    
    // set up the typechecker
    let mut tc = typechecker::Typechecker::new();
    tc.import_prelude(&prelude);
    
    // parse some terms, yay
    let ast = parser::parse_snack(&contents);
    if ast.is_err() {
        println!("parse error: {:#?}", ast);
        return Ok(());
    }

    let ast = ast.unwrap();
    // and then typecheck them
    for term in &ast {
	let ty = tc.typecheck(term.clone());
        if let Err(e) = ty {
            println!("{}", e);
            return Ok(());
        }
    }

    // Now it's time to compile the program, with help from the symbol table that the
    // typechecker made.
    let mut compiler = compiler::Compiler::new(&fpath, tc.get_symtab(), tc.get_locals());
    compiler.import_prelude(&prelude);
    compiler.compile_program(&ast);

    Ok(())
}
