/**
 * Copyright (C) 2020 by Benjamin Duchild
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use polytype::Type;
use jvm_assembler::Java as JavaType;

/// A method import with a name from java, and a snack-style name/type.
#[derive(Debug, Clone)]
pub struct MethodImport<'a> {
    pub name: String,
    pub class: String,
    pub snack_type: Type,
    pub java_params: Vec<JavaType<'a>>,
    pub java_ret: JavaType<'a>,
}

/// Represents a loadable set of functionality
/// that can be imported into snack by default.
pub struct Prelude<'a> {
    pub methods: Vec<MethodImport<'a>>
}

impl<'a> Prelude<'a> {
    pub fn new() -> Prelude<'a> {
        Prelude { methods: Vec::new() }
    }
    
    pub fn add_method(&mut self, name: String, class: String, snack_type: Type,
                      java_params: Vec<JavaType<'a>>, java_ret: JavaType<'a>) {
        let method_import = MethodImport{name, class, snack_type, java_params, java_ret};
        self.methods.push(method_import);
    }
}
