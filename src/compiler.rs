/**
 * Copyright (C) 2020 by Benjamin Duchild
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use jvm_assembler::*;
use jvm_assembler::Java as JavaType;
use polytype::Type;
use crate::util::*;
use crate::symtab::*;
use crate::typechecker::LocalMap;
use crate::prelude::*;
use std::path::Path;
use std::collections::HashMap;

/// A struct for holding information on a method that might be relevant when compiling it.
struct MethodInfo<'a> {
    name: String,
    returns: JavaType<'a>,
}

// This should really be abstracted more, but I want to make a working compiler before I
// refactor everything,
// since refactoring a program like this could go on for potentially forever.
/// A compiler that holds a source file's AST and other info, and can transform it into JVM
/// bytecode. This turns the source program, which is essentially just a list of declarations,
/// into a single JVM class file. Because Snack (as of yet) doesn't support records or
/// classes/interfaces, all the declarations can just be turned into static declarations inside
/// the class. Nice! Anyway, if the snack program has a `main` function, this will be turned
/// into into the class' public static void main(String[] args).
pub struct Compiler<'a> {
    /// The name of the class (file) being compiled.
    file_name: &'a str,
    /// The symbol table, derived from typechecking to program.
    symtab: &'a SymbolTable,
    /// A list of local variable types and numberings.
    locals: &'a LocalMap,
    /// A record of which local variable names correspond to which numbers in the bytecode
    /// (the numbers are what's used in load_n instructions (load_0, load_1, etc))
    local_numbers: HashMap<String, u16>,
    /// A record of how many local variables have been declared as of the current point in the
    /// method.
    local_count: Counter,
    /// A record of all the imported methods.
    imports: HashMap<String, MethodImport<'a>>
}

#[allow(dead_code)]
impl<'a> Compiler<'a> {
    pub fn new(path: &'a Path, symtab: &'a SymbolTable, locals: &'a LocalMap) -> Compiler<'a> {
	Compiler {
	    file_name: path.file_stem().unwrap().to_str().unwrap(),
            symtab: symtab,
            locals: locals,
            local_numbers: HashMap::new(),
            local_count: Counter::new(),
            imports: HashMap::new(),
        }
    }
    
    /// Compile a program! Since functions aren't implemented yet, everything goes inside the
    /// main method.
    pub fn compile_program(&mut self, terms: &Vec<Term>) {
        use Term::*;
        
        // Define a new public class with the source program's name.
	// It extends the java/lang/Object class by default.
        let mut class = define_class(ACC_PUBLIC, &self.file_name, "java/lang/Object");

        // Every term on the top level of the AST should be a function definition.
        // Functions get compiled into static methods.
        for term in terms {
            match &*term {
                Define(name, val) =>
                    if let Fn(ref params, ref body) = **val {
                        // look up the declared function's name in the symbol table
                        let sym = self.symtab.retrieve_symbol(&name).unwrap();
                        let fn_ty = sym.get_ty();
                        // Since the typechecker understands functions to be curried, the
                        // parameter list will need to be "flattened out".
                        let components = &Self::flatten_fn_ty(fn_ty.clone());
                        // get its type and dissect that to find the parameter and return types
                        let (param_types, return_type) = match fn_ty {
                            Type::Constructed("→", _) => {
                                // Collect the "component" types; all but the last are
                                // parameters (the last is the return type).
                                let mut ps = Vec::new();
                                let mut i = 0;
                                for ty in components {
                                    let jty = Self::snack_type_to_java_type(&ty);
                                    // unless it's the last (return) type, all `void` types in
                                    // snack are just
                                    // placeholders (really they're used as a unit type), so
                                    // they can be omitted
                                    if i == (components.len()-1) || jty != JavaType::Void {
                                        ps.push(jty);
                                    }
                                    
                                    i += 1;
                                }
                                
                                let ret = ps.pop().unwrap();
                                (ps, ret)
                            },

                            _ => panic!("expected function type, but got type `{}`", fn_ty),
                        };

                        // count each parameter as a local variable
                        for p in params {
                            self.add_local(p);
                        }
                        
                        // define the method (kind of like writing a method header):
                        let mut method = class.define_method(ACC_PUBLIC|ACC_STATIC, &name,
                                                             &param_types, &return_type);
                        let info = MethodInfo {
                            name: name.to_string(),
                            returns: return_type,
                        };
                        // go through the method body and compile it
                        for t in body {
                            self.compile_term(&mut method, &info, &t);
                        }

                        // The last term of the function is what gets returned, sooooo
                        // just match on the return type to choose what /type/ of return
                        // instruction to use, since the value should already be pushed onto
                        // the stack by now.
                        match info.returns {
                            JavaType::Int | JavaType::Boolean | JavaType::Char
                                => method.ireturn(),
                            JavaType::Float => method.freturn(),
                            JavaType::Void => method.do_return(),
                            _ => method.areturn(),
                        }

                        // reset the local variable counter
                        self.local_count.reset();
                        // finish the method definition
                        method.done();
                    },
                
                _ => panic!("Found a term other than function at top level"),
            }            
        }
        
	// we're also done with the class
	let classfile = class.done();
	// We didn't do all this for nothing! Save it in a .class file so we can trick the JVM
        // into thinking this was a Java program all along and not written in some weird
        // language called "Snack".
        let fname = Path::new(&self.file_name).with_extension("class");
	write_classfile(classfile, fname.as_path().to_str().unwrap());
    }

    pub fn import_prelude(&mut self, prelude: &'a Prelude) {
        for method in &prelude.methods {
            self.imports.insert(method.name.clone(), method.clone());
        }
    }

    fn add_local(&mut self, name: &String) -> u16 {
        let n = self.local_count.inc();
        self.local_numbers.insert(name.clone(), n);
        n
    }

    fn get_local_num(&mut self, name: &String) -> Option<&u16> {
        self.local_numbers.get(name)
    }

    fn get_local(&self, fname: &String, vname: &String) -> &Type {
        self.locals.get(fname).unwrap()
            .get(vname).unwrap()
    }
    
    /// Converts a snack type to a Java type.
    fn snack_type_to_java_type(ty: &Type) -> JavaType {
        // note: this doesn't work for function types yet since first-class functions are
        // sketchier in Java.
        match ty {
            Type::Constructed("i32", _) => JavaType::Int,
            Type::Constructed("f32", _) => JavaType::Float,
            Type::Constructed("bool", _) => JavaType::Boolean,
            Type::Constructed("char", _) => JavaType::Char,
            Type::Constructed("void", _) => JavaType::Void,
            Type::Constructed("str", _) => JavaType::Class("java/lang/String"),
            Type::Constructed("list", _) => JavaType::Class("java/util/ArrayList"),
            _ => JavaType::Class("java/lang/Object"), // generics???
        }
    }

    /// Converts a polytype-style function type into a list of parameter types (with the return
    /// type at the end). This is necessary because polytype curries functions with multiple
    /// parameters, which isn't easy to iterate through at all.
    fn flatten_fn_ty(ty: Type) -> Vec<Type> {
        let mut types = Vec::new();

        match ty {
            Type::Constructed("→", next) =>
                for ty in next {
                    let mut flattened = Self::flatten_fn_ty(ty);
                    types.append(&mut flattened);
                }
            
            not_a_fn => types.push(not_a_fn),
        }

        types
    }

    fn compile_term(&mut self, method: &mut MethodBuilder,
                    info: &MethodInfo, term: &Term) {
        use Term::*;
        
        match term {
            I32(n) => self.compile_i32(method, *n),
            F32(n) => self.compile_f32(method, *n),
            Char(c) => self.compile_char(method, *c),
            Str(s) => method.load_constant(s.as_str()),
            Bool(b) => self.compile_bool(method, *b),
            // `void` doesn't really do anything and only occurs at the end of functions.
            // It's only encountered before a do_return, so nothing really needs to be done.
            Void => (),
            Nil => method.aconst_null(),

            List(terms) => {
                // create a new arraylist reference, duplicate it for future use,
                // and then initialize it
                method.nyew("java/util/ArrayList");
                method.dup();
                method.invoke_virtual("java/util/ArrayList", "<init>", &[], &JavaType::Void);
                // push each term in the list
                for t in terms {
                    self.compile_term(method, info, &*t);
                    method.invoke_virtual("java/util/ArrayList", "add",
                                          &[JavaType::Class("java/lang/Object")],
                                          &JavaType::Void);
                }
            }
            
            Define(name, val) => {
                // first, compile the value being defined
                self.compile_term(method, info, &val);
                // add it to the local variable counter and save its number
                let n = self.add_local(name) as u8;
                let local_snack_ty = self.get_local(&info.name, &name);
                let local_ty = Self::snack_type_to_java_type(local_snack_ty);
                
                match local_ty {
                    JavaType::Int | JavaType::Boolean | JavaType::Char =>
                        self.store_i32(method, n),
                    JavaType::Float => self.store_f32(method, n),
                    _ => self.store_ref(method, n),
                }
            },
            
            Ident(name) => {
                // look up a local variable
                let maybe_local = self.get_local_num(&name);
                
                // this branch executes if the name indeed refers to a local variable
                if maybe_local.is_some() {
                    let n = maybe_local.unwrap().clone() as u8;
                    let local_snack_ty = self.get_local(&info.name, &name);
                    let local_ty = Self::snack_type_to_java_type(local_snack_ty);
                    
                    match local_ty {
                        JavaType::Int | JavaType::Boolean | JavaType::Char =>
                            self.load_i32(method, n),
                        JavaType::Float => self.load_f32(method, n),
                        _ => self.load_ref(method, n),
                    }

                } else {
                    // This branch would be executed when a function is referred to by name
                    // outside the context of application, i.e., in a situation where it's
                    // treated as first-class.
                    println!("Snack doesn't support first-class functions.");
                }
            },
            
            App(func, args) => {
                match **func {
                    Builtin(ref op) => self.compile_builtin_application(method, info, op, args),
                    // this is for function calls
                    Ident(ref fn_name) => {
                        // compile the arguments
                        for arg in args {
                            self.compile_term(method, info, &arg);
                        }

                        // handle static (user-defined) functions
                        // all functions are declared at the top level, so looking this up
                        // should be no problem
                        if let Some(func) = self.symtab.retrieve_symbol(&fn_name) {
                            // and its type should be a function type, which means it should get
                            // flattened for further use
                            let mut fn_ty = Self::flatten_fn_ty(func.get_ty().clone());
                            // convert the types to Java types, starting with the return
                            let ret_ty = fn_ty.pop().unwrap();
                            let ret_ty = Self::snack_type_to_java_type(&ret_ty);
                            // now convert the parameter types
                            let mut param_tys = Vec::new();
                            for param_ty in &fn_ty {
                                let java_ty = Self::snack_type_to_java_type(param_ty);
                                param_tys.push(java_ty);
                            }

                            // Invoke the (static) method, using its name and type, as well as
                            // the name of the class we're compiling.
                            method.invoke_static(self.file_name, &fn_name,
                                                 param_tys.as_slice(), &ret_ty);
                        } else {
                            let import = self.imports.get(fn_name).unwrap();
                            let class = import.class.clone();
                            let name = import.name.clone();
                            let param_tys = import.java_params.clone();
                            let ret_ty = import.java_ret.clone();
                            
                            method.invoke_virtual(&class, &name, &param_tys, &ret_ty);
                        }
                    }
                     
                    _ => panic!("Expected function but got something else"),
                }
            },

            Term::Cond(if_branches, else_branch) => {
                // create "environments" to distinguish the jump points
                // for each branch and for the end of the chain
                let mut branch_envs: Vec<u16> = Vec::new();
                for _ in 0..=if_branches.len() {
                    branch_envs.push(method.new_env());
                }
                let end_env = method.new_env();

                // iterate through the if branches and link them
                let mut i = 0;
                for (cond, body) in if_branches {
                    // label the start of the branch
                    method.set_env(branch_envs[i]);
                    method.label("bbranch");
                    // compile the conditional
                    self.compile_term(method, info, cond);
                    // emit a conditional jump to the "begin"
                    // that will be generated for the next branch
                    method.set_env(branch_envs[i+1]);
                    method.ifeq("bbranch");
                    // compile the body
                    for term in body {
                        self.compile_term(method, info, term);
                    }

                    // jump to the end of the chain
                    method.set_env(end_env);
                    method.goto("ebranches");
                    i += 1;
                }

                // label the else branch
                method.set_env(branch_envs[i]);
                method.label("bbranch");
                // compile its body
                for term in else_branch {
                    self.compile_term(method, info, term);
                }

                method.set_env(end_env);
                method.label("ebranches");
            }
            
            _ => panic!("Could not compile term `{}', aka `{:?}'", term, term),
        }
    }
    
    fn compile_builtin_application(&mut self, method: &mut MethodBuilder,
                                   info: &MethodInfo, op: &Op, args: &Vec<Box<Term>>) {
        match op {
            Op::Add => {
                self.compile_term(method, info, &args[0]);
                self.compile_term(method, info, &args[1]);
                method.iadd();
            }
            
            Op::Sub => {
                self.compile_term(method, info, &args[0]);
                self.compile_term(method, info, &args[1]);
                method.isub();
            },

            Op::Mul => {
                self.compile_term(method, info, &args[0]);
                self.compile_term(method, info, &args[1]);
                method.imul();
            },

            Op::Div => {
                self.compile_term(method, info, &args[0]);
                self.compile_term(method, info, &args[1]);
                method.idiv();
            },

            Op::Rem => {
                self.compile_term(method, info, &args[0]);
                self.compile_term(method, info, &args[1]);
                method.irem();
            },

            Op::Fadd => {
                self.compile_term(method, info, &args[0]);
                self.compile_term(method, info, &args[1]);
                method.fadd();
            }
            
            Op::Fsub => {
                self.compile_term(method, info, &args[0]);
                self.compile_term(method, info, &args[1]);
                method.fsub();
            },

            Op::Fmul => {
                self.compile_term(method, info, &args[0]);
                self.compile_term(method, info, &args[1]);
                method.fmul();
            },

            Op::Fdiv => {
                self.compile_term(method, info, &args[0]);
                self.compile_term(method, info, &args[1]);
                method.fdiv();
            },

            Op::Frem => {
                self.compile_term(method, info, &args[0]);
                self.compile_term(method, info, &args[1]);
                method.frem();
            },
            
            Op::I2C => {
                self.compile_term(method, info, &args[0]);
                method.i2c();
            },

            Op::I2F => {
                self.compile_term(method, info, &args[0]);
                method.i2f();
            },
            
            Op::F2I => {
                self.compile_term(method, info, &args[0]);
                method.f2i();
            },
            
            // Boolean operators, surprisingly, compile into conditional branches.
            // The comments help explain what goes on for each operator.
            Op::Not => {
                let n = method.set_new_env();
                // compile the argument (let's call it `b`)
                self.compile_term(method, info, &args[0]);
                method.set_env(n);
                // if b != 0 (ie b == true) goto @false
                method.ifne("false");
                method.iconst1(); // push 1 (true)
                method.goto("end"); // goto @end
                method.label("false"); // @false
                method.iconst0(); // push 0 (false)
                method.label("end"); // @end
            }
            
            Op::And => {
                let n = method.set_new_env();
                // compile the first argument (let's call it `a`)
                self.compile_term(method, info, &args[0]);
                // if a == 0 goto @false
                method.set_env(n);
                method.ifeq("false");
                // compile the second argument (let's call it `b`)
                self.compile_term(method, info, &args[1]);
                // if b == 0 goto @false
                method.set_env(n);
                method.ifeq("false");
                method.iconst1(); // push 1 (true)
                method.goto("end"); // goto @end
                method.label("false"); // @false
                method.iconst0(); // push 0 (false)
                method.label("end"); // @end
            }
            
            Op::Or => {
                let n = method.set_new_env();
                // compile the first argument (let's call it `a`)
                self.compile_term(method, info, &args[0]);
                // if a != 0 (ie a == true) goto @true
                method.set_env(n);
                method.ifne("true");
                // compile the second argument (let's call it `b`)
                self.compile_term(method, info, &args[1]);                
                // if b == 0 goto @false
                method.set_env(n);
                method.ifeq("false");
                method.label("true"); // @true
                method.iconst1(); // push 1 (true)
                method.goto("end"); // goto @end
                method.label("false"); // @false
                method.iconst0(); // push 0 (false)
                method.label("end"); // @end
            }

            Op::Eq => {
                let n = method.set_new_env();
                // compile the first argument (let's call it `a`)
                self.compile_term(method, info, &args[0]);
                // compile the second argument (let's call it `b`)
                self.compile_term(method, info, &args[1]);
                // if a != b goto @false
                method.set_env(n);
                method.if_icmp_ne("false");
                method.iconst1(); // push 1 (true)
                method.goto("end"); // goto @end
                method.label("false"); // @false
                method.iconst0(); // push 0 (false)
                method.label("end"); // @end
            }

            Op::Neq => {
                let n = method.set_new_env();
                // compile the first argument (let's call it `a`)
                self.compile_term(method, info, &args[0]);
                // compile the second argument (let's call it `b`)
                self.compile_term(method, info, &args[1]);
                // if a == b goto @false
                method.set_env(n);
                method.if_icmp_eq("false");
                method.iconst1(); // push 1 (true)
                method.goto("end"); // goto @end
                method.label("false"); // @false
                method.iconst0(); // push 0 (false)
                method.label("end"); // @end
            }
            
            Op::Gt => {
                let n = method.set_new_env();
                // compile the first argument (let's call it `a`)
                self.compile_term(method, info, &args[0]);
                // compile the second argument (let's call it `b`)
                self.compile_term(method, info, &args[1]);
                // if b <= a goto @false
                method.set_env(n);
                method.if_icmp_le("false");
                method.iconst1(); // push 1 (true)
                method.goto("end"); // goto @end
                method.label("false"); // @false
                method.iconst0(); // push 0 (false)
                method.label("end"); // @end
            }

            Op::Gte => {
                let n = method.set_new_env();
                // compile the first argument (let's call it `a`)
                self.compile_term(method, info, &args[0]);
                // compile the second argument (let's call it `b`)
                self.compile_term(method, info, &args[1]);
                // if b < a goto @false
                method.set_env(n);
                method.if_icmp_lt("false");
                method.iconst1(); // push 1 (true)
                method.goto("end"); // goto @end
                method.label("false"); // @false
                method.iconst0(); // push 0 (false)
                method.label("end"); // @end
            }

            
            Op::Lt => {
                let n = method.set_new_env();
                // compile the first argument (let's call it `a`)
                self.compile_term(method, info, &args[0]);
                // compile the second argument (let's call it `b`)
                self.compile_term(method, info, &args[1]);
                // if b >= a goto @false
                method.set_env(n);
                method.if_icmp_ge("false");
                method.iconst1(); // push 1 (true)
                method.goto("end"); // goto @end
                method.label("false"); // @false
                method.iconst0(); // push 0 (false)
                method.label("end"); // @end
            }

            Op::Lte => {
                let n = method.set_new_env();
                // compile the first argument (let's call it `a`)
                self.compile_term(method, info, &args[0]);
                // compile the second argument (let's call it `b`)
                self.compile_term(method, info, &args[1]);
                // if b >= a goto @false
                method.set_env(n);
                method.if_icmp_gt("false");
                method.iconst1(); // push 1 (true)
                method.goto("end"); // goto @end
                method.label("false"); // @false
                method.iconst0(); // push 0 (false)
                method.label("end"); // @end
            }

            // _ => panic!("The compiler has not implemented this operator: `{}`", op),
        }
    }

    fn load_i32(&mut self, method: &mut MethodBuilder, reg: u8) {
        match reg {
            0 => method.iload0(),
            1 => method.iload1(),
            2 => method.iload2(),
            3 => method.iload3(),
            _ => method.iload(reg),
        }
    }

    fn store_i32(&mut self, method: &mut MethodBuilder, reg: u8) {
        match reg {
            0 => method.istore0(),
            1 => method.istore1(),
            2 => method.istore2(),
            3 => method.istore3(),
            _ => method.istore(reg),
        }
    }

    fn compile_i32(&mut self, method: &mut MethodBuilder, n: i32) {
        if n.abs() < 0x80 {
            match n {
                -1 => method.iconstm1(),
                0 => method.iconst0(),
                1 => method.iconst1(),
                2 => method.iconst2(),
                3 => method.iconst3(),
                4 => method.iconst4(),
                5 => method.iconst5(),
                _ => method.bipush(n as i8),
            }
        } else if n.abs() < 0x8000 {
            // get two bytes out of `n`
            let byte0 = n & 0x00ff;
            let byte1 = n >> 8;
            method.sipush(byte1 as i8, byte0 as i8);
        } else {
            method.load_constant_integer(n);
        }
    }

    fn compile_bool(&mut self, method: &mut MethodBuilder, b: bool) {
        match b {
            false => method.iconst0(),
            true => method.iconst1(),
        }
    }

    fn compile_char(&mut self, method: &mut MethodBuilder, c: char) {
        let mut buf = [0; 4];
        c.encode_utf16(&mut buf);
        let n = ((buf[3] as u32) << 8)
            | ((buf[2] as u32) << 8)
            | ((buf[1] as u32) << 8)
            | (buf[0] as u32);
        self.compile_i32(method, n as i32);
    }

    fn load_f32(&mut self, method: &mut MethodBuilder, reg: u8) {
        match reg {
            0 => method.fload0(),
            1 => method.fload1(),
            2 => method.fload2(),
            3 => method.fload3(),
            _ => method.fload(reg),
        }
    }

    fn store_f32(&mut self, method: &mut MethodBuilder, reg: u8) {
        match reg {
            0 => method.fstore0(),
            1 => method.fstore1(),
            2 => method.fstore2(),
            3 => method.fstore3(),
            _ => method.fstore(reg),
        }
    }

    fn compile_f32(&mut self, method: &mut MethodBuilder, n: f32) {
        if n == n.round() {
            match n as i32 {
                0 => method.fconst0(),
                1 => method.fconst1(),
                2 => method.fconst2(),
                _ => method.load_constant_float(n),
            }
        }
    }

    fn load_ref(&mut self, method: &mut MethodBuilder, reg: u8) {
        match reg {
            0 => method.aload0(),
            1 => method.aload1(),
            2 => method.aload2(),
            3 => method.aload3(),
            _ => method.aload(reg),
        }
    }

    fn store_ref(&mut self, method: &mut MethodBuilder, reg: u8) {
        match reg {
            0 => method.astore0(),
            1 => method.astore1(),
            2 => method.astore2(),
            3 => method.astore3(),
            _ => method.astore(reg),
        }
    }
}
