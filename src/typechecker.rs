/**
 * Copyright (C) 2020 by Benjamin Duchild
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use polytype::{ptp, tp, Context, Type, TypeSchema};
use crate::errors::{TypeError, ErrorKind};
use crate::util::*;
use crate::symtab::*;
use crate::prelude::*;
use std::clone::Clone;
use std::collections::HashMap;

pub type LocalMap = HashMap<String, HashMap<String, Type>>;

/// A `Typechecker` is able to determine the type of a term, and can remember variables in its
/// type context.
/// Typechecking works via Hindley-Milner unification.
pub struct Typechecker {
    /// A counter to track how many variables have been defined.
    var_count: Counter,
    /// The typechecking context.
    ctx: Context,
    /// A structure to keep track of currently defined symbols.
    symtab: SymbolTable,
    /// A map that keeps track of the locally defined variables in each function.
    locals: LocalMap,
    /// Whether or not variables can be overwritten. In snack, you can't redefine a variable
    /// in the same scope, but the typechecker's internal logic sometimes requires that
    /// variables are updated within the symbol table so their types are more accurate.
    allow_overwrites: bool,
}

impl Typechecker {
    /// Constructs a new typechecker with an empty context and one empty scope level in the
    /// symbol table.
    pub fn new() -> Typechecker {
        let mut table = SymbolTable::new();
        table.open_scope();
        
        Typechecker {
            var_count: Counter::new(),
            ctx: Context::default(),
            symtab: SymbolTable::new(),
            locals: HashMap::new(),
            allow_overwrites: false,
        }
    }

    pub fn import_prelude(&mut self, prelude: &Prelude) {
        for method in &prelude.methods {
            self.add_var(&method.name.clone(), method.snack_type.clone()).unwrap();
        }
    }

    /// Gets a reference to the typechecker's symbol table
    pub fn get_symtab(&self) -> &SymbolTable {
        &self.symtab
    }

    /// Gets a reference to the typechecker's local variable map
    pub fn get_locals(&self) -> &LocalMap {
        &self.locals
    }

    /// Determines the type of a term, using the context of variables the typechecker has built
    /// up. This function may result in a `TypeError`.
    pub fn typecheck(&mut self, t: Term) -> Result<Type, TypeError> {
	// `t` gets (partially) moved in the match statement below, so it'll
	// need to be cloned in case we need to move it to a `TypeError` struct.
	let terr = t.clone();
        
        // different terms require different typechecking strategies
        match t {
            // literals are obvious
            Term::I32(_) => Ok(tp!(i32)),
            Term::F32(_) => Ok(tp!(f32)),
            Term::Bool(_) => Ok(tp!(bool)),
            Term::Char(_) => Ok(tp!(char)),
            Term::Str(_) => Ok(tp!(str)),
	    Term::Void => Ok(tp!(void)),
            Term::Nil => Ok(self.ctx.new_variable()),

            // lists have to hold elements that are all the same type.
            Term::List(items) => {
                let mut item_types = Vec::new();
                for item in items {
                    let ty = self.typecheck(*item)?;
                    item_types.push(ty);
                }
                
                let ret_ty: Result<Type, TypeError> = item_types.iter()
                    .fold(Ok(self.ctx.new_variable()), |acc, ty| {
                        if acc.is_err() { return acc; }
                        let acc = acc.unwrap();
                        
                        match self.ctx.unify(&acc, ty) {
                            Err(e) => Err(TypeError::new(terr.clone(),
                                                         ErrorKind::UnificationError(acc.clone(), ty.clone(), e))),
                            _ => Ok(acc.apply(&self.ctx)),
                        }
                    });

                match ret_ty {
                    Ok(ty) => Ok(tp!(list(ty))),
                    _ => ret_ty,
                }
            }
            
	    // For definitions, typecheck the value and then add it under the variable's name
            // to the type
            // context. Definition terms themselves will always return `void`.
            Term::Define(name, val) => {
                let ty = if let Term::Fn(_, _) = *val {
                    // add a dummy declaration for this function to allow recursion
                    // let dummy_ty = self.ctx.new_variable();
                    // self.add_var(&name, dummy_ty.clone())?;
                    // typecheck the function body
                    //let ty = self.tc_fn(&*val)?.0;
                    // define the function with its *real* type this time, and typecheck it
                    // a second time to ensure that any recursive calls respect its real type
                    //self.enable_overwrites();
                    //self.add_var(&name, ty.clone())?;
                    let (ty, locals) = self.tc_fn(&*val)?;
                    self.add_var(&name, ty.clone())?;
                    self.locals.insert(name.clone(), locals);
                    ty
                } else {
                    let ty = self.typecheck(*val)?;
                    self.add_var(&name, ty.clone())?;
                    ty
                };
                
                // this is for debugging btw
                self.disable_overwrites();
                println!("[DEBUG] {} : {}", name, ty);
                Ok(tp!(void))
            },
            
            // If the variable referred to by `ident` exists, just return its type from the
            // symbol table.
            Term::Ident(name) =>
                match self.get_var_ty(&name) {
                    Some(ty) => Ok(ty),
                    None => {
                        let n = name.to_string();
                        Err(TypeError::new(terr, ErrorKind::UndefinedVar(n)))
                    }
                }
	    
            
            Term::Fn(_, _) => Ok(self.tc_fn(&t)?.0),

            // yikes
            Term::Cond(if_branches, else_branch) => {
                // for keeping track of what type each branch 'returns'
                let mut branch_types: Vec<Type> = Vec::new();
                // iterate through the if branches
                for (condition, body) in if_branches {
                    // first of all, the condition /must/ unify to a boolean type
                    let cond_ty = self.typecheck(*condition)?;
                    match self.ctx.unify(&cond_ty, &tp!(bool)) {
                        Err(err) =>
                            return Err(TypeError::new(terr, ErrorKind::UnificationError(cond_ty, tp!(bool), err))),
                        _ => (),
                    }

                    // typecheck all the statements in the body,
                    // and save the final type
                    let mut i = 0;
                    for term in &body {
                        let ty = self.typecheck(*term.clone())?;
                        
                        if i == (body.len() - 1) {
                            branch_types.push(ty);
                        }
                        
                        i += 1;
                    }
                }
                
                // now typecheck the body if the else branch
                let mut i = 0;
                for term in &else_branch {
                    let ty = self.typecheck(*term.clone())?;
                    
                    if i == (else_branch.len() - 1) {
                        branch_types.push(ty);
                    }
                    
                    i += 1;
                }

                // all the branches should 'return' the same types, so doing a weird
                // unify-fold type of thing to the list of branch types should yield
                // their common type, or start screaming and error if something's up
                let ret_ty: Result<Type, TypeError> = branch_types.iter()
                    .fold(Ok(self.ctx.new_variable()), |acc, ty| {
                        if acc.is_err() { return acc; }
                        let acc = acc.unwrap();
                        
                        match self.ctx.unify(&acc, ty) {
                            Err(e) => Err(TypeError::new(terr.clone(),
                                      ErrorKind::UnificationError(acc.clone(), ty.clone(), e))),
                            _ => Ok(acc.apply(&self.ctx)),
                        }
                    });

                ret_ty
            }

	    Term::App(abs, args) => {
                // Get the type of the abstraction being applied;
	        let abs_ty1 = match *abs {
                    Term::Builtin(op) => Self::tc_op(&op).instantiate(&mut self.ctx),
                    _ => self.typecheck(*abs)?
                };

                // then, typecheck the actual arguments and use these types
                // (along with a new type variable for the return type) to construct
                // another version of the abstraction's supposed type.
                let mut arg_tys = Vec::new();
                for arg in args {
                    let ty = self.typecheck(*arg)?;
                    arg_tys.push(ty);
                }
                
	        let ret = self.ctx.new_variable();
                arg_tys.push(ret.clone());                
                let abs_ty2 = Type::from(arg_tys);
                
                // Unify these together, and then apply the substitutions on the
                // type variable from earlier to deduce the return type.
                match self.ctx.unify(&abs_ty1, &abs_ty2) {
                    Err(err) =>
                        return Err(TypeError::new(terr,
						  ErrorKind::UnificationError(abs_ty1, abs_ty2,
                                                                              err))),
                    _ => (),
                }

	        Ok(ret.apply(&self.ctx))
	    }
            
            // if the term somehow can't be typechecked, return an error
            _ => Err(TypeError::new(terr, ErrorKind::Unimplemented)),
        }
    }

    /// All operator types are already known, so this method just determines them using a
    /// `match` statement.
    fn tc_op(op: &Op) -> TypeSchema {
        match op {
            Op::Add | Op::Sub | Op::Mul | Op::Div | Op::Rem
                => ptp!(@arrow[tp!(i32), tp!(i32), tp!(i32)]),
            Op::Fadd | Op::Fsub | Op::Fmul | Op::Fdiv | Op::Frem
                => ptp!(@arrow[tp!(f32), tp!(f32), tp!(f32)]),
            Op::And | Op::Or
                => ptp!(@arrow[tp!(bool), tp!(bool), tp!(bool)]),
            Op::Lt | Op::Lte | Op::Gt | Op::Gte
                => ptp!(@arrow[tp!(i32), tp!(i32), tp!(bool)]),
            Op::Not => ptp!(@arrow[tp!(bool), tp!(bool)]),
            // note that some operators work on multiple types, so they use type variables
            Op::Eq | Op::Neq => ptp!(0; @arrow[tp!(0), tp!(0), tp!(bool)]),
            Op::I2C => ptp!(@arrow[tp!(i32), tp!(char)]),
            Op::I2F => ptp!(@arrow[tp!(i32), tp!(f32)]),
            Op::F2I => ptp!(@arrow[tp!(f32), tp!(i32)]),
        }
    }

    /// A method for typechecking an abstraction. It's special because it also returns a list
    /// of all local variable bindings from the function, because those get erased once
    /// the symbol table exits the function's scope.
    fn tc_fn(&mut self, t: &Term) -> Result<(Type, HashMap<String, Type>), TypeError> {
        let terr = t.clone();
        
        match t {
            Term::Fn(params, body) => {
                // this is a new scope btw
                self.symtab.open_scope();
	        // Make type variables for the function's parameters, and add these parameters
                // into the symbol table.
                for param in params {
		    let v = self.ctx.new_variable();
                    self.add_var(&param, v)?;
	        }
                
                let mut _retty = None;
                let mut i = 0;
                for term in body {
                    let ty = self.typecheck(term.clone())?;
                    // the last value in a function is what gets returned, so the function's
                    // return type should be equal to that.
                    if i == body.len()-1 {
                        _retty = Some(ty);
                    }

                    i += 1;
                }
                
                // a return type has to exist, so unwrap it
                let retty = _retty.unwrap();

	        // Now that all that happened, we *should* have all the substitutions
                // necessary to determine parameter types.
                self.enable_overwrites();
	        let mut fn_types = Vec::new();
	        for param in params {
                    let sym = self.symtab.retrieve_symbol(param).unwrap();
		    let nice_ty = sym.get_ty().apply(&self.ctx);
                    self.add_var(param, nice_ty.clone())?;
		    fn_types.push(nice_ty);
                }

                self.disable_overwrites();
                
                // if there are *no* parameters, add an implicit parameter type of `void`
                if fn_types.is_empty() {
                    fn_types.push(tp!(void));
                }

                // add the return type onto the end of the function's type
	        fn_types.push(retty);
                
                // goodbye scope
                let locals = self.symtab.close_scope();
                // println!("[DEBUG] locals: {:?}", locals);
	        // and now, create the arrow type for the function and return it.
	        Ok((Type::from(fn_types), locals))
	    }

            _ => Err(TypeError::new(terr, ErrorKind::Unimplemented)),
        }
    }

    /// Add a variable/type definition to the symbol table. The symbol table's internal logic
    /// will check to see if said variable has already been defined, so this method may throw
    /// a `TypeError` if variable overwrites are disabled.
    fn add_var(&mut self, name: &String, ty: Type) -> Result<(), TypeError> {
        self.symtab.enter_symbol(name, ty.clone(), self.allow_overwrites)?;

        let v = self.var_count.inc();
        self.ctx.extend(v, ty);

        Ok(())
    }

    fn enable_overwrites(&mut self) {
        self.allow_overwrites = true;
    }

    fn disable_overwrites(&mut self) {
        self.allow_overwrites = false;
    }
    
    /// Look up a variable's type from the symbol table. This method
    /// returns an `Option` because the variable may not exist.
    fn get_var_ty(&mut self, name: &String) -> Option<Type> {
	let sym = self.symtab.retrieve_symbol(name);

	match sym {
	    Some(sym) => Some(sym.get_ty().clone()),
	    None => None,
	}
    }
}
