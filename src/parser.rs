/**
 * Copyright (C) 2020 by Benjamin Duchild
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::pest::Parser;
use pest::error::Error;
use pest::iterators::Pair;

use crate::util::*;

#[derive(Parser)]
#[grammar = "snack.pest"]
pub struct SnackParser;

pub fn parse_snack(src: &str) -> Result<Vec<Term>, Error<Rule>> {
    let mut terms = vec![];
    let pairs = SnackParser::parse(Rule::snack, src)?;

    for pair in pairs {
        let t = parse_term(pair);
        if let Some(term) = t {
            terms.push(term);
        }
    }

    Ok(terms)
}

fn parse_term(pair: Pair<Rule>) -> Option<Term> {
    match pair.as_rule() {
        Rule::int => Some(Term::I32(pair.as_str().parse().unwrap())),
        Rule::float => Some(Term::F32(pair.as_str().parse().unwrap())),
        Rule::boolean => Some(Term::Bool(parse_bool(pair.as_str()))),
	Rule::void => Some(Term::Void),
        Rule::nil => Some(Term::Nil),
        
        Rule::list => {
            let pair = pair.into_inner();
            let mut terms: Vec<Box<Term>> = Vec::new();

            for term in pair {
                let t = parse_term(term).unwrap();
                terms.push(Box::new(t));
            }

            Some(Term::List(terms))
        }
        
        Rule::ch => {
            // get the part of the string that isn't the two tick (') quotes
            let s = pair.as_str();
            let len = s.len();
            let s: String = s.chars().skip(1).take(len-2).collect();
            let s: &str = s.as_str();

            Some(Term::Char(parse_char(s)))
        }
        
        Rule::string => {
            // all the escaped characters in a string must be processed
            let mut pair = pair.into_inner();
            let unprocessed_string = pair.next().unwrap().as_str();
            let chars: Vec<char> = unprocessed_string.chars().collect();
            let mut processed_string = String::new();

            // go through each char in the string
            let mut i = 0;
            while i < chars.len() {
                let ch = chars[i];
                // some 'chars' are composed of multiple (parsed) chars, like
                // escape codes, so some lookahead stuff is needed here
                let mut s = String::new();
                s.push(ch);
                // consume the character after the escape
                if ch == '\\' {
                    i += 1;
                    let nxt = chars[i];
                    s.push(nxt);
                    // if the next character is a 'u', that means it's a unicode escape
                    // so there are four more characters in the sequence.
                    if nxt == 'u' {
                        i += 1; s.push(chars[i]);
                        i += 1; s.push(chars[i]);
                        i += 1; s.push(chars[i]);
                        i += 1; s.push(chars[i]);
                    }
                }

                // processed the char add it to the result
                let c = parse_char(&s);
                processed_string.push(c);
                i += 1;
            }

            Some(Term::Str(processed_string))
        }

        Rule::binary_app => {
            let mut pair = pair.into_inner();
            let operand1 = parse_term(pair.next().unwrap()).unwrap();
            let operator = parse_op(pair.next().unwrap().as_str());
            let operand2 = parse_term(pair.next().unwrap()).unwrap();
            
            Some(Term::App(Box::new(operator), vec![Box::new(operand1), Box::new(operand2)]))
        }
        
        Rule::other_app => {
	    let mut pair = pair.into_inner();
	    let operator = parse_op(pair.next().unwrap().as_str());
	    let mut operands = Vec::new();
            
            for oprnd in pair {
                let t = parse_term(oprnd).unwrap();
                operands.push(Box::new(t));
            }
            
	    Some(Term::App(Box::new(operator), operands))
        }

        Rule::ident => {
	    let name = pair.as_str().to_string();
	    Some(Term::Ident(name))
	}

        Rule::define => {
            let mut pair = pair.into_inner();
	    let name = match parse_term(pair.next().unwrap()).unwrap() {
		Term::Ident(s) => s,
		_ => unreachable!(),
	    };
            
            let val = parse_term(pair.next().unwrap()).unwrap();

            Some(Term::Define(name, Box::new(val)))
        }

        Rule::func => {
            let mut pair = pair.into_inner();
            let name = match parse_term(pair.next().unwrap()).unwrap() {
		Term::Ident(s) => s,
		_ => unreachable!(),
	    };
            // Collect the parameter names and body.
            // The first list of idents comprises the parameters, and if the very last term is
            // an ident too, then that's part of the body. Otherwise, everything after the
            // first idents is the body.
            let mut params = Vec::new();
            let mut body = Vec::new();
            // whether the parser is done looking at the param list
            let mut after_params = false;
            for ident in pair {                
                match parse_term(ident).unwrap() {
		    Term::Ident(ref s) if !after_params => params.push(s.clone()),
		    something_else => {
                        if !after_params { after_params = true; }
                        body.push(something_else);
                    }
	        };
            }
            
            // if everything is an ident, then the last one must be the returned value of
            // the function (a function like `id(x) -> { x }` would have this issue), so
            // check for that
            if body.is_empty() {
                let returned_ident = params.pop().unwrap();
                body.push(Term::Ident(returned_ident));
            }
            
            Some(Term::Define(name, Box::new(Term::Fn(params, body))))
        }

        // yikes
        Rule::if_stmt => {
            fn parse_body(body_pair: Pair<'_, Rule>) -> Vec<Box<Term>> {
                // uh oh this it a little complex
                // basically, iterate through the body and grab all the terms,
                // and then put them in boxes and store them
                let mut boxed_body: Vec<Box<Term>> = Vec::new();
                for pair in body_pair.into_inner() {
                    let term = parse_term(pair).unwrap();
                    boxed_body.push(Box::new(term));
                }

                boxed_body
            }
            
            let mut pair = pair.into_inner();
            let mut branches = Vec::new();
            let mut _else_branch = None;

            // look through the tokens, parsing pairs of conditions and bodies
            while let Some(p) = pair.next() {
                if p.as_rule() == Rule::condition {
                    let cond = parse_term(p).unwrap();
                    let body_pair = pair.next().unwrap();
                    let boxed_body = parse_body(body_pair);
                    branches.push((Box::new(cond), boxed_body));
                } else {
                    // if the rule isn't `condition`, i.e., a `body` was encountered right
                    // after another `body`, then that second `body` must belong to the
                    // final `else` branch.
                    // parsing follows the same process as in the above branch.
                    let boxed_body = parse_body(p);
                    _else_branch = Some(boxed_body);
                    break;
                }
            }
            
            if _else_branch.is_none() {
                panic!("Parse error: if statement lacks and else branch");
            }

            let else_branch = _else_branch.unwrap();

            Some(Term::Cond(branches, else_branch))
        }
        
        // these are "wrapper" rules that represent categories of terms, so just parse what's
        // inside.
        Rule::expr | Rule::term | Rule::val | Rule::condition =>
            parse_term(pair.into_inner().next().unwrap()),
        
        _ => None,
    }
}


/// Parses the str representation of a boolean into an actual boolean.
fn parse_bool(s: &str) -> bool {
    match s {
        "^t" => true,
        "^f" => false,
        _ => panic!("Not a boolean"),
    }
}

/// Parses the str representation of a char into an actual char.
fn parse_char(s: &str) -> char {
    // based on how long the string is, parse it into a char
    if s.len() == 1 {
        // handling normal chars
        return s.chars().next().unwrap();
    } else if s.len() == 2 {
        // handling simple escaped chars
        match s {
            "\\t" => return '\t',
            "\\n" => return '\n',
            "\\r" => return '\r',
            "\\'" => return '\'',
            "\\\"" => return '\"',
            "\\\\" => return '\\',
            _ => (),
        };
    } else if s.len() == 6 {
        // handlings \uXXXX-style escapes
        // chop off the "\u" part
        let s: String = s.chars().skip(2).collect();
        // the escape code stands for a two-byte value
        // (but this is a u32 because `std::char::from_u32` is used later)
        let mut bytes: u32 = 0;
        
        // iterate through the string and construct `bytes`
        // by appending each hex value in the escape sequence
        for ch in s.chars() {
            bytes <<= 4; // since a hex digit = 4 bits
            let val = ch.to_digit(16).unwrap();
            bytes |= val;
        }
        
        return std::char::from_u32(bytes).unwrap();
    }

    // something must have gone pretty wrong if this is reached
    panic!("Error: cannot convert '{}' to a char.", s);
}

/// Tries to parse the argument into an operator, and parses it into an ident if that doesn't
/// work.
fn parse_op(s: &str) -> Term {
    match s {
        "+" => Term::Builtin(Op::Add),
        "-" => Term::Builtin(Op::Sub),
        "*" => Term::Builtin(Op::Mul),
        "/" => Term::Builtin(Op::Div),
        "%" => Term::Builtin(Op::Rem),
        "+." => Term::Builtin(Op::Fadd),
        "-." => Term::Builtin(Op::Fsub),
        "*." => Term::Builtin(Op::Fmul),
        "/." => Term::Builtin(Op::Fdiv),
        "%." => Term::Builtin(Op::Frem),
        "||" => Term::Builtin(Op::Or),
        "&&" => Term::Builtin(Op::And),
        "!" => Term::Builtin(Op::Not),
        "==" => Term::Builtin(Op::Eq),
        "!=" => Term::Builtin(Op::Neq),
        ">" => Term::Builtin(Op::Gt),
        ">=" => Term::Builtin(Op::Gte),
        "<" => Term::Builtin(Op::Lt),
        "<=" => Term::Builtin(Op::Lte),
        "%i2c" => Term::Builtin(Op::I2C),
        "%i2f" => Term::Builtin(Op::I2F),
        "%f2i" => Term::Builtin(Op::F2I),
        _ => Term::Ident(s.to_string()),
    }
}
