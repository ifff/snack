/**
 * Copyright (C) 2020 by Benjamin Duchild
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use polytype::Type;
use std::fmt;
use crate::util::*;

#[derive(Debug)]
pub struct TypeError {
    term: Term,
    kind: ErrorKind,
}

#[derive(Debug)]
pub enum ErrorKind {
    DuplicateDef,
    UnificationError(Type, Type, polytype::UnificationError),
    UndefinedVar(String),
    Unimplemented,
}

impl TypeError {
    pub fn new(term: Term, kind: ErrorKind) -> TypeError {
	TypeError { term: term, kind: kind }
    }
}

impl fmt::Display for TypeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
	let mut msg = format!("Error typechecking `{}` :\n\t", self.term);
	
	match &self.kind {
            ErrorKind::DuplicateDef =>
                msg.push_str("Duplicate definitions are prohibited within the same scope."),
	    ErrorKind::UnificationError(t1, t2, err) =>
		msg.push_str(format!("Could not unify types {} and {}. Detailed error message:\n\t{}",
                                     t1, t2, err).as_str()),
	    ErrorKind::UndefinedVar(name) =>
		msg.push_str(format!("The variable `{}` is not defined.", name).as_str()),
            ErrorKind::Unimplemented =>
                msg.push_str("Typechecking is not implemented for this term."),
	}

	write!(f, "{}", msg)
    }
}
