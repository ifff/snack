/**
 * Copyright (C) 2020 by Benjamin Duchild
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use polytype::Type;
use std::collections::HashMap;
use std::rc::Rc;
use std::fmt;
use crate::errors::{TypeError, ErrorKind};
use crate::util::Term;

pub struct SymbolTable {
    hashtable: SymHash,
    scope_display: Vec<Option<Rc<Symbol>>>,
    depth: usize,
}

#[allow(dead_code)]
impl SymbolTable {
    pub fn new() -> SymbolTable {
        SymbolTable {
	    hashtable: SymHash::new(),
            scope_display: vec![None; 10],
	    depth: 0,
        }
    }

    /// Open a new scope.
    pub fn open_scope(&mut self) {
        // increase the depth
	self.depth += 1;
        // increase the size of the scope display if needed
	if self.scope_display.len() == self.scope_display.capacity() {
	    self.scope_display.resize(self.scope_display.len() + 10, None);
	}
    }

    /// Close a scope by pruning off the symbols that have fallen out of scope,
    /// and return them in a `HashMap` in case they're needed for something later.
    pub fn close_scope(&mut self) -> HashMap<String, Type> {
        // get the first entry in this scope
        // (ie the head of the linked list of symbols in the current scope)
	let mut entry = &self.scope_display[self.depth];
        // this is where we'll store the symbols after they get culled
        let mut out_of_scope = HashMap::new();

        // go through the linked list
	while entry.is_some() {
            // find the symbol with the current entry's name that resides in the previous scope
            // (it may or may not exist), and save it
            let sym = entry.as_ref().unwrap();
	    let prev = sym.next_of_name();
            // now delete the current entry from the hashtable
	    self.hashtable.delete(sym);
            // save the symbol we removed, but only if a symbol with its name
            // isn't in the saves yet (there's a minor bug where overwritten symbols are
            // still linked by level, because it would be very inefficient to remove those)
            let name = sym.get_name();
            if !out_of_scope.contains_key(name) {
                let ty = sym.get_ty().clone();
                // println!("[DEBUG] exporting `{}`", sym);
                out_of_scope.insert(name.clone(), ty);
            }
            
            // if the previous symbol exists, it's now safe to add it to the hashtable
	    if prev.is_some() {
                let prv = prev.as_ref().unwrap();
		self.hashtable.add(&prv);
	    }
            
            // now look at the next symbol in the current scope (for the next iteration)
	    entry = sym.next_of_level();
	}

        // there shouldn't be any linking at this depth anymore
        self.scope_display[self.depth] = None;
        // all the symbols in the current scope are dead and gone, so now it's safe
        // to say that we're in the previous scope
	self.depth -= 1;
        // return the symbols that fell out of scope
        return out_of_scope;
    }

    /// Look up a symbol from the current scope by name.
    pub fn retrieve_symbol(&self, name: &String) -> Option<&Rc<Symbol>> {
        // look up the entry in the hashtable
        let mut entry = self.hashtable.get(name);

        // some names (well I don't think this really happens in rust) may hash to
        // the same value, so handle collisions by iterating through the linked list by hash
        // and looking for matches by name
	while entry.is_some() {
            let sym = entry.unwrap();
	    if sym.get_name() == name {
		return Some(sym);
	    }

	    entry = sym.next_of_hash().as_ref();
	}

	return None;
    }

    /// Enter a symbol with a type into the symbol table in the current scope.
    pub fn enter_symbol(&mut self, name: &String, ty: Type, can_overwrite: bool)
                        -> Result<(), TypeError> {
        // see if there's already a symbol with this name
	let old_sym: Option<&Rc<Symbol>> = self.retrieve_symbol(name);

        // if there is, and it's declared in the current scope depth, freak out
        if !can_overwrite && old_sym.is_some() {
            let sym: &Rc<Symbol> = old_sym.unwrap();
            
            if sym.get_depth() == self.depth {
                return Err(TypeError::new(Term::Ident(name.clone()),
                                          ErrorKind::DuplicateDef));
            }
        }

        // otherwise make a symbol
	let mut new_sym: Symbol = Symbol::new(name.to_string(), ty, self.depth);
        // get the start of the linked list of symbols at this depth (may not exist)
        let next_of_level: &Option<Rc<Symbol>> = &self.scope_display[self.depth];
        // if it does exist, link it to our new symbol unless it shares its name with the
        // new symbol, in which case we'd be overwriting it so its existence no longer matters
        if next_of_level.is_some() {
            let next: &Rc<Symbol> = next_of_level.as_ref().unwrap();
            new_sym.link_next_level(&next);
        }
        
        // and if we are overwriting a symbol, link it to the new one by name
        // but! if the old sym has the same depth, then we must be overwriting it, so in
        // that case, don't link it
	if old_sym.is_some() {
            let sym: &Rc<Symbol> = old_sym.unwrap();
            // previous checks should ensure that `sym` shares the new symbol's name,
            // and that overwriting is allowed
            if sym.get_depth() != self.depth {
                new_sym.link_next_name(sym);
            }
	}

        // add this symbol to the hash table and scope display
        let new_rc: Rc<Symbol> = Rc::new(new_sym);
        self.hashtable.add(&new_rc);
        self.scope_display[self.depth] = Some(new_rc);
        // wow it worked, everything worked fine, we're good
        Ok(())
    }
}

struct SymHash {
    table: HashMap<String, Rc<Symbol>>
}

#[allow(dead_code)]
impl SymHash {
    fn new() -> SymHash {
        SymHash { table: HashMap::new() }
    }
    
    fn add(&mut self, sym: &Rc<Symbol>) {
        let name = sym.get_name().to_string();
        self.table.insert(name, Rc::clone(&sym));
    }

    fn get(&self, name: &String) -> Option<&Rc<Symbol>> {
        self.table.get(name)
    }

    fn delete(&mut self, sym: &Symbol) {
        self.table.remove(sym.get_name());
    }
}

/// A `Symbol` represents a variable.
#[derive(Clone, Debug, PartialEq)]
pub struct Symbol {
    /// The symbol's name.
    name: String,
    /// The symbol's type.
    ty: Type,
    hash: Option<Rc<Symbol>>,
    /// A link to a symbol with this name in the next outer scope,
    /// if such a symbol exists.
    var: Option<Rc<Symbol>>,
    /// A link to the next symbol declared at this level, if such a symbol exists.
    level: Option<Rc<Symbol>>,
    /// The depth of nesting at which this symbol was declared.
    depth: usize,
}

impl fmt::Display for Symbol {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
	write!(f, "{}@{} : {}", self.name, self.depth, self.ty)
    }
}


#[allow(dead_code)]
impl Symbol {
    pub fn new(name: String, ty: Type, depth: usize) -> Symbol {
        Symbol {
            name: name,
            ty: ty,
            hash: None,
            var: None,
            level: None,
            depth: depth,
        }
    }

    pub fn get_name(&self) -> &String {
        &self.name
    }

    pub fn get_ty(&self) -> &Type {
        &self.ty
    }

    pub fn next_of_name(&self) -> &Option<Rc<Symbol>> {
        &self.var
    }

    pub fn next_of_level(&self) -> &Option<Rc<Symbol>> {
        &self.level
    }

    pub fn next_of_hash(&self) -> &Option<Rc<Symbol>> {
	&self.hash
    }

    pub fn get_depth(&self) -> usize {
        self.depth
    }

    pub fn link_next_name(&mut self, next: &Rc<Symbol>) {
        self.var = Some(Rc::clone(next));
    }
    
    pub fn link_next_level(&mut self, next: &Rc<Symbol>) {
        self.level = Some(Rc::clone(&next));
    }

    pub fn link_next_hash(&mut self, next: &Rc<Symbol>) {
	self.hash = Some(Rc::clone(&next));
    }
}
