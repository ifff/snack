public class FnCall {
    public static int call() {
        return add(1, 2);
    }

    public static int add(int x, int y) {
        return x + y;
    }
}
