public class Generics {
    public static void main(String[] args) {
	String s = id("hello");
	int n = id(20);
    }

    public static <T> T id(T thing) {
	return thing;
    }
}
