public class Chars {
    public static void main(String[] args) {
        char c = 'A';
        char d = c;

        // here's how to concatenate two bytes (defined as shorts to make things simpler)
        // short ad = (short)0xad;
        // short de = (short)0xde;
        // short de_ad = (short)((de << 8) | ad);
        // char dead = '\udead';

        // System.out.println("de_ad: " + (char)de_ad + "; dead: " + dead);
	System.out.println(bigChar());
    }

    public static char a() {
	return 'a';
    }

    public static char bigChar() {
        return '\u1a01';
    }
}
