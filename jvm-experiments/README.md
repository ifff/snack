# Some JVM Experiments
The directory holds experiments I'm working on in order to understand how JVM bytecode works. If somebody
has erased your memory and you can't remember how to play with bytecode, here's how:
```bash
# compile a source file
javac Name.java

# output bytecode in readable format
javap -c Name

# run the code
java Name
```

This might be of use:
[jvm-assmbler crate](https://github.com/kenpratt/jvm-assembler/blob/master/examples/hello_world.rs).
