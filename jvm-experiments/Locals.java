public class Locals {
    public static int foo(int x) {
        if (x == 2) {
            int z = 5;
            int a = z + 1;
        } else {
            int z = 6;
            int a = z - 1;
        }

        return x;
    }
}
