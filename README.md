# Snack #

Snack is an experimental programming language that compiles to JVM bytecode.

## feature progress/planning checklist ##

### base functionality for: ###

[x] parser
[x] typechecker
[x] compiler

### language elements ###

implemented for {p = parser, t = typechecker, c = compiler}
[x] i32 p/t/c
[x] bool p/t/c
[x] + - operators p/t/c
[x] * / % operators p/t/c
[x] floats p/t/c
[x] float operators p/t/c
[x] all boolean operators p/t
[x] declarations p/t/c
[x] invocations p/t/c
[x] functions p/t/c
[x] conditionals
[x] strings p/t/c
